//
//  TravelRepublicTests.swift
//  TravelRepublicTests
//
//  Created by Maninder Soor on 18/01/2017.
//  Copyright © 2017 Travel Republic. All rights reserved.
//

import XCTest
@testable import TravelRepublic

class TravelRepublicTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAPI () {
        
        /// Fetch from local
        API.instance.fetchOfflineData { (result) in
            switch result {
            case let .Failure((type, message)):
                XCTFail("Database couldn't update \(type) with message \(message)")
            case let .Success ((deals)):
                XCTAssertEqual(deals.count, 3)
            }
        }
        
    }
    
}
