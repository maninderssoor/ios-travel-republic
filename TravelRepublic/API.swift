//
//  API.swift
//  TravelRepublic
//
//  Created by Maninder Soor on 18/01/2017.
//  Copyright © 2017 Travel Republic. All rights reserved.
//

import Foundation
import Alamofire
import XCGLogger

/**
The api class handles all connections to a networking layer
*/
class API {
	
	///	Singleton instance
	static let instance = API()
	
	///	Logger
	let log = XCGLogger.default
	
	/** 
		Fetch data from the API and return Deal Objects in the closure
	*/
	func fetchData (completion : @escaping (_ result : APIResult<[APIDeal]>) -> Void){
		
		///	The URL
		let urlString = "https://www.travelrepublic.co.uk/api/hotels/deals/search?fields=Aggregates.HotelsByChildDestination"
		
		/// Parameters
		let parameters : [String : Any] = ["CheckInDate":"2017-01-10T00:00:00.000Z",
		                                   "Flexibility":3,
		                                   "Duration":7,
		                                   "Adults":2,
		                                   "DomainId":1,
		                                   "CultureCode":"en-gb",
		                                   "CurrencyCode":"GBP",
		                                   "OriginAirports":["LHR","LCY","LGW","LTN","STN","SEN"],
		                                   "FieldFlags":8143571,
		                                   "IncludeAggregates":true]

		log.verbose("Fetching API data with URL \(urlString) and parameters \(parameters)")
		
		///	Request
		let request = Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
			
			///	Debug print the response
			debugPrint(response)
			
			///	Log
			self.log.debug("API result with status code \(response.response?.statusCode) and response JSON \(response.result.value)")
			
			///	Check the status code and that it's a 200
			guard let statusCode = response.response?.statusCode, statusCode == 200  else {
				self.log.error("Bad response from the server when fetching data")
				completion(APIResult.Failure((failureType: .emptyResponse, message: "")))
				return
			}
			
			///	Parse the JSON into Deals objects to pass back in the completion
			
		}
		
		///	Debug
		debugPrint(request)
	}
	
    
    /** 
        Fetch offline data
     */
    func fetchOfflineData (completion : @escaping (_ result : APIResult<[APIDeal]>) -> Void){
        
        /// Sample response
        let sample_response : [String : Any] = ["HotelsByChildDestination": [
            "10|10030": [
                "Title": "Majorca, Ibiza & Menorca",
                "Count": 178,
                "MinPrice": 428.96,
                "Position": 1   // Use this field to order the list.
            ],
            "10|10031": [
                "Title": "Canary Islands",
                "Count": 106,
                "MinPrice": 555.00,
                "Position": 0
            ],
            "10|10049": [
                "Title": "Spain",
                "Count": 99,
                "MinPrice": 49.99,
                "Position": 2
            ]
        ]]
        
        /// Process into data set
        guard let deals = self.processResponse(response: sample_response) else {
            log.error("No deals were returned")
            completion(APIResult.Failure((failureType: .emptyResponse, message: "No Deals")))
            return
        }
        
        /// Return the deals
        completion(APIResult.Success(deals))
    }
    
    /** 
        Process the JSON response
     */
    func processResponse (response : [String : Any]) -> [APIDeal]? {
        
        /// Check for results
        guard let hotels = response["HotelsByChildDestination"] as? [String : Any] else {
            self.log.error("A HotelsByChildDestination value wasn't found from the response")
            return nil
        }
        
        /// Deals array
        var deals = [APIDeal]()
        
        /// Loop each to create an array of Deals
        for thisDeal in hotels {
            
            /// Log
            self.log.verbose("Processing this deal \(thisDeal)")
            
            /// Get the type and uuid
            let uuid_split = thisDeal.key.components(separatedBy: "|")
            
            /// Check for all the data
            /// Type
            guard let type = uuid_split.first else {
                log.error("Couldnt find type on deal \(thisDeal)")
                continue
            }
            /// UUID
            guard let uuid = uuid_split.last else {
                log.error("Couldn't find uuid on deal")
                continue
            }
            /// Results
            guard let thisDealValue = thisDeal.value as? [String : Any] else {
                log.error("Couldn't find this deals value")
                continue
            }
            /// Title
            guard let title = thisDealValue["Title"] as? String else {
                log.error("Couldn't find a Title on deal")
                continue
            }
            /// Count
            guard let count = thisDealValue["Count"] as? Int else {
                log.error("Couldn't find a Count on deal")
                continue
            }
            /// MinPrice
            guard let minPrice = thisDealValue["MinPrice"] as? Double else {
                log.error("Couldn't find a MinPrice on deal")
                continue
            }
            let formatter = NumberFormatter()
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 2
            guard let minPriceNumber = formatter.string(from: NSNumber(value: minPrice)) else {
                log.error("Couldn't format the MinPrice deal")
                continue
            }
            
            /// Position
            guard let position = thisDealValue["Position"] as? Int else {
                log.error("Couldn't find a Position on deal")
                continue
            }
            
            /// Append to the deals dictionary
            deals.append(APIDeal(uuid: uuid, type: type, title: title, count: Int16(count), minimumPrice: NSDecimalNumber(string: minPriceNumber ), position: Int16(position)))
            
        }
        
        /// Return the deals
        return deals
    }
}

/**
Generic API response
*/
enum APIResult<T> {
	
	/**
	When there is a failure in the API call it will return a closure with parameters
	
	- parameter failureType: failureType
	- parameter message: String of the error when calling the API
	**/
	case Failure((failureType: APIResultFailureType, message: String))
	
	/// When the API call is successfull it will return a type <T>
	case Success(T)
}


/**
	Enums for APIResultFailureType
*/
enum APIResultFailureType {
	
	///	Bad URL
	case urlError
	
	///	No Response
	case emptyResponse
	
}

/** 
 API Deal objects
 */
struct APIDeal {
    /// UUID
    let uuid : String?
    
    /// Type
    let type : String?
    
    /// Title
    let title : String?
    
    /// Count
    let count : Int16

    /// Min Price
    let minimumPrice : NSDecimalNumber?
    
    /// Postition
    let position : Int16
    
    /** 
     Basic setup
     */
    init (uuid : String?, type : String?, title: String?, count: Int16, minimumPrice : NSDecimalNumber?, position: Int16) {
        self.uuid = uuid
        self.type = type
        self.title = title
        self.count = count
        self.minimumPrice = minimumPrice
        self.position = position
    }
    
}


extension Deal {
    
    func imageURL (width: Int, height: Int) -> String? {
        if let uuid = uuid, let type = type {
            return "https://d2f0rb8pddf3ug.cloudfront.net/api2/destination/images/getfromobject?id=\(uuid)&type=\(type)&useDialsImages=true&width=\(width)&height=\(height)"
        }
        return nil
    }
    
}
