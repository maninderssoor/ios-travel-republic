//
//  Database.swift
//  TravelRepublic
//
//  Created by Maninder Soor on 18/01/2017.
//  Copyright © 2017 Travel Republic. All rights reserved.
//

import Foundation
import XCGLogger
import CoreData

/** 
	The database class handles all connections to Core Data
*/
class Database {
	
	///	Singleton instance
	static let instance = Database()
	
	///	Logger
	let log = XCGLogger.default
	
	/** 
		Updated data and saving to the database
	
		The completion closure notifies if it was successful
	*/
	func updateData (completion : @escaping (_ isComplete : Bool) -> Void) {
		
		///	Call the API instance
		API.instance.fetchOfflineData { (result) in
			switch result {
			case let .Failure((type, message)):
                self.log.error("Database couldn't update \(type) with message \(message)")
                completion(false)
            case let .Success ((deals)):
                self.log.error("Database retrieved deals \(deals)")
                /// For each of the deals
                for thisDeal in deals {
                    
                    /// If it's in the DB, update
                    if let uuid = thisDeal.uuid,
                        let fetchedDeal = self.fetchFromDB(uuid: uuid) {
                        
                        /// Update the values
                        self.updateInDB(using: fetchedDeal, deal: thisDeal)
                        
                    }
                    /// Else insert
                    else {
                        self.insertToDB(deal: thisDeal)
                    }
                    
                }
                completion(true)
			}
		}
		
		
	}
    
    /// The deals fetched results controller
    lazy var dealsFetchedResultsController: NSFetchedResultsController<Deal> = {
        // Create Fetch Request
        let fetchRequest: NSFetchRequest<Deal> = Deal.fetchRequest()
        
        // Configure Fetch Request
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "position", ascending: true)]
        
        // Create Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return fetchedResultsController
    }()
    
    // MARK: - Core Data stack
    
    /**
     Core Data insertion
     */
    func insertToDB (deal : APIDeal ) {
        
        if let entity = NSEntityDescription.entity(forEntityName: "Deal", in: self.managedObjectContext) {
            let newDeal = Deal(entity: entity, insertInto: self.managedObjectContext)
            newDeal.uuid = deal.uuid
            newDeal.type = deal.type
            newDeal.title = deal.title
            newDeal.count = deal.count
            newDeal.minimumPrice = deal.minimumPrice
            newDeal.position = deal.position
            
            /// Save context
            self.saveContext()
        }
        
    }
    
    func fetchFromDB (uuid : String) -> Deal? {
        /// Fetch request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Deal")
        
        /// Predicate
        let predicate = NSPredicate(format: "uuid = \(uuid)")
        fetchRequest.predicate = predicate
        
        /// Run the fetch
        do {
            return try self.managedObjectContext.fetch(fetchRequest).first as? Deal
        } catch let error as NSError {
            self.log.error("Error fetching from DB with uuid \(uuid) and error \(error)")
            return nil
        }
    }
    
    func updateInDB (using dbDeal : Deal, deal : APIDeal) {
        
        /// Values changed
        var changed = false
        
        /// Update the values if different
        if let dbType = dbDeal.type, let dealType = deal.type, dbType != dealType {
            dbDeal.type = deal.type
            changed = true
        }
        if let dbTitle = dbDeal.title, let dealTitle = deal.title, dbTitle != dealTitle {
            dbDeal.title = deal.title
            changed = true
        }
        if dbDeal.count != deal.count {
            dbDeal.count = deal.count
            changed = true
        }
        if let dbMinPrice = dbDeal.minimumPrice, let dealMinPrice = deal.minimumPrice, dbMinPrice != dealMinPrice {
            dbDeal.minimumPrice = deal.minimumPrice
            changed = true
        }
        if dbDeal.position != deal.position {
            dbDeal.position = deal.position
            changed = true
        }
        
        if changed {
            self.saveContext()
        }
    }
    
    // Applications default directory address
    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "TravelRepublic", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("TravelRepublic.sqlite")
        do {
            // If your looking for any kind of migration then here is the time to pass it to the options
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch let  error as NSError {
            print("Ops there was an error \(error.localizedDescription)")
            abort()
        }
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the
        // application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to
        // fail.
        let coordinator = self.persistentStoreCoordinator
        var context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.persistentStoreCoordinator = coordinator
        return context
    }()
    
    // if there is any change in the context save it
    func saveContext() {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch let error as NSError {
                print("Ops there was an error \(error.localizedDescription)")
                abort() 
            }
        } 
    }

}
