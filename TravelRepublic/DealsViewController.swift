//
//  DealsViewController.swift
//  TravelRepublic
//
//  Created by Maninder Soor on 18/01/2017.
//  Copyright © 2017 Travel Republic. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Kingfisher
import CoreData
import XCGLogger

/** 
	The Deals View Controller
*/
class DealsViewController : UIViewController {
	
	///	Overall loader
	@IBOutlet weak var loader : UIActivityIndicatorView?
    
    /// Logger
    let log = XCGLogger.default
    
	///	The Table View
	@IBOutlet weak var tableView : UITableView?
	
    /// Images cached
    var downloadedIndicies : [IndexPath] = [IndexPath]()
    
    /// Fetch results controller
    let controller = Database.instance.dealsFetchedResultsController
    
	/** 
		View did load setup
	*/
	override func viewDidLoad() {
		super.viewDidLoad()
		
        /// Remove pull to referesh to iOS 10
        if #available(iOS 10.0, *) {
            self.tableView?.refreshControl = nil
        }
        
		///	Update data and setup the view
		self.setupData()
	}
	
    /** 
        Setup data
     */
    func setupData () {
        Database.instance.updateData { (complete) in
            if complete {
                self.setup(isLoading: false)
                
                /// Fetch request and delegate
                do {
                    try self.controller.performFetch()
                    self.controller.delegate = self
                } catch {
                    let fetchError = error as NSError
                    self.log.error("Unable to Perform Fetch Request")
                    self.log.error("\(fetchError), \(fetchError.localizedDescription)")
                }
            } else {
                let alertController = UIAlertController(title: "Error", message: "Error downloading data.", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "O.K.", style: .destructive, handler: nil)
                let retryAction = UIAlertAction(title: "Retry", style: .default, handler: { (_) in
                    self.setupData()
                })
                alertController.addAction(retryAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
	/**
		Setup for the view
	*/
	func setup(isLoading : Bool) {
		UIView.animate(withDuration: 0.3) {
			self.loader?.alpha = isLoading ? 1.0 : 0.0
			self.tableView?.alpha = isLoading ? 0.0 : 1.0
		}
	}
    
    
}

extension DealsViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.controller.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = self.controller.sections {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DealTableViewCell.identifier, for: indexPath) as? DealTableViewCell
        let data = self.controller.object(at: indexPath)
        /// Setup
        cell?.setup(with: data)
        
        /// Download image, if not already
        if !self.downloadedIndicies.contains(indexPath) {
            self.downloadedIndicies.append(indexPath)
            
            /// Download the image
            if let imageURL = data.imageURL(width: 1000, height: 1000), let url = URL(string: imageURL) {
                let resource = ImageResource(downloadURL: url)
                cell?.imageBackground?.kf.setImage(with: resource, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cache, url) in
                    cell?.imageBackground?.image = image
                    cell?.setImageLoaded(state: true)
                })
            } else {
                cell?.setImageLoaded(state: false)
            }
        }
        /// Return
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        /// Remove from index
        if let index = self.downloadedIndicies.index(of: indexPath) {
            self.downloadedIndicies.remove(at: index)
        }
    }
}

extension DealsViewController : UITableViewDelegate {
    
}

extension DealsViewController : NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView?.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView?.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView?.insertRows(at: [indexPath as IndexPath], with: .automatic)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView?.deleteRows(at: [indexPath as IndexPath], with: .automatic)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                self.tableView?.reloadRows(at: [indexPath as IndexPath], with: .automatic)
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView?.deleteRows(at: [indexPath as IndexPath], with: .automatic)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView?.insertRows(at: [newIndexPath as IndexPath], with: .automatic)
            }
            break;
        }
    }
    
}

/** 
    Deals table view cell for the list of deals
 */
class DealTableViewCell : UITableViewCell {
    
    /// The identifier
    static let identifier = "DealTableViewCellIdentifier"
    
    /// The deal object
    private var deal : Deal?
    
    /// Activity loader
    @IBOutlet weak var activity : UIActivityIndicatorView?
    
    /// The background image
    @IBOutlet weak var imageBackground : UIImageView?
    
    /// The Title
    @IBOutlet weak var labelTitle : UILabel?
    
    /// Title width layout constraint
    @IBOutlet weak var titleWidthConstraint : NSLayoutConstraint?
    
    /// The count
    @IBOutlet weak var labelCount : UILabel?
    
    ///  The price
    @IBOutlet weak var labelPrice : UILabel?
    
    /** 
        Setup
     */
    func setup (with deal : Deal) {
        /// Set local
        self.deal = deal
        
        /// Show activity
        self.setImageLoaded(state: false)
        
        /// Set values
        self.labelTitle?.text = deal.title?.uppercased()
        self.labelCount?.text = "(\(deal.count))"
        self.labelPrice?.text = "£\(deal.minimumPrice ?? NSDecimalNumber.zero)"
        
        /// Set width of title
        self.titleWidthConstraint?.constant = self.labelTitle?.intrinsicContentSize.width ?? 100.0
    
    }
    
    func setImageLoaded(state isLoaded : Bool) {
        self.activity?.startAnimating()
        UIView.animate(withDuration: 0.3, animations: {
            self.activity?.alpha = isLoaded ? 0.0 : 1.0
            self.labelTitle?.alpha = isLoaded ? 1.0 : 0.0
            self.imageBackground?.alpha = isLoaded ? 1.0 : 0.0
            self.labelCount?.alpha = isLoaded ? 1.0 : 0.0
            self.labelPrice?.alpha = isLoaded ? 1.0 : 0.0
        })
    }
    
}
